# EasyOpen--支持新版Eclipse快速打开选中文件目录



## EasyOpen项目简易说明

由于easyexplorer等插件长期未更新，在新版的eclipse已经无法使用
抽了点时间开发了个适合新版本的快速打开文件所在文件夹的eclips插件

没什么技术含量，改了个名字叫EasyOpen，怕写的不好，影响了EasyExplorer

本地测试了windows，mac，deepin linux 三个平台。理论上可以全平台通用


# 插件安装：

* **简易方式：直接把插件jar包丢到eclipse的dropins目录，然后删掉configuration目录下的org.eclipse.update文件夹，重启eclipse即可**

* **如果想偷懒，懒得自己编译的话，从附件里边下载一个吧**

1.0.1版本，处理某些Linux桌面环境没得nautilus导致无法打开的问题，改成了gnome-open打开文件夹


附效果图：可以在工程或者代码上直接右键，Easy Open -> Open Folder


![输入图片说明](http://git.oschina.net/uploads/images/2015/1228/101307_5ebe8678_113326.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2015/1228/101345_61020650_113326.png "在这里输入图片标题")
